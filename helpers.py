from rest_framework.pagination import PageNumberPagination

class Pagination(object):
	
	class DefaultPagination(PageNumberPagination):
	    page_size = 20
	    max_page_size = 20