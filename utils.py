import datetime

class DateTimeConverter(object):

    def to_str_with_tz(date_time):
        """
        Converts date-time object to string containing T-Z
        
        Args:
            date_time (datetime): DateTime object
        Returns:
            obj (str): DateTime string
        """
        return date_time.strftime('%Y-%m-%dT%H:%M:%SZ')

    def to_date_time(date_time):
        """
        Converts date-time string to date-time object

        Args:
            date_time (str): DateTime string

        Return:
            obj (datetime): DateTime object
        """
        return datetime.datetime.strptime(
            date_time.split('T')[0] + ' ' + date_time.split('T')[1].split('Z')[0],
            '%Y-%m-%d %H:%M:%S')
