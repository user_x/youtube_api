from rest_framework import serializers

from video_tracker import models


class VideoSerializer(serializers.ModelSerializer):
    thumbnails = serializers.SerializerMethodField()

    def get_thumbnails(self, instance):
        """
        SerializerMethodField: Setting thumbnail(s) for each Video
        """
        return [VideoThumbnailSerializer(thumbnail).data
            for thumbnail in models.VideoThumbnail.objects.filter(video=instance)]

    class Meta:
        model = models.Video
        fields = '__all__'


class VideoThumbnailSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.VideoThumbnail
        fields = '__all__'


class APIConfigurationSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.APIConfiguration
        fields = '__all__'