# Generated by Django 2.2 on 2021-02-16 02:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='APIConfiguration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.TextField()),
                ('expired', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': 'API Configuration',
                'verbose_name_plural': 'API Configurations',
            },
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('title', models.TextField()),
                ('description', models.TextField()),
                ('published_at', models.DateTimeField()),
            ],
            options={
                'verbose_name': 'Video',
                'verbose_name_plural': 'Videos',
            },
        ),
        migrations.CreateModel(
            name='VideoThumbnail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('url', models.CharField(max_length=2083)),
                ('video', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='thumbnail', to='video_tracker.Video')),
            ],
            options={
                'verbose_name': 'Video Thumbnail',
                'verbose_name_plural': 'Video Thumbnails',
            },
        ),
    ]
