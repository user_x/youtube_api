from django.db import models


class CreateUpdateModel(models.Model):
	created_at = models.DateTimeField(auto_now_add=True, editable=False)
	updated_at = models.DateTimeField(auto_now=True, editable=False)

	class Meta:
		abstract = True


class APIConfiguration(CreateUpdateModel):
    key = models.TextField()
    expired = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'API Configuration'
        verbose_name_plural = 'API Configurations'

    def __str__(self):
        return self.key

class Video(CreateUpdateModel):
    title =  models.TextField()
    description =  models.TextField()
    published_at =  models.DateTimeField(auto_now=False)

    class Meta:
        verbose_name = 'Video'
        verbose_name_plural = 'Videos'

    def __str__(self):
        return self.title


class VideoThumbnail(CreateUpdateModel):
    video = models.ForeignKey(Video, on_delete=models.CASCADE, related_name="thumbnail")
    url = models.CharField(max_length=2083)

    class Meta:
        verbose_name = 'Video Thumbnail'
        verbose_name_plural = 'Video Thumbnails'

    def __str__(self):
        return self.video.title