from django.contrib import admin

from video_tracker import models


@admin.register(models.Video)
class VideoAdmin(admin.ModelAdmin):
	list_display = [field.name for field in models.Video._meta.fields]
	search_fields = ('published_at', 'title', 'id',)
	list_filter = ('published_at',)


@admin.register(models.VideoThumbnail)
class VideoThumbnailAdmin(admin.ModelAdmin):
	list_display = [field.name for field in models.VideoThumbnail._meta.fields]
	search_fields = ('video__title', 'video__published_at', 'video__id', 'id',)
	list_filter = ('video',)


@admin.register(models.APIConfiguration)
class APIConfigurationAdmin(admin.ModelAdmin):
	list_display = [field.name for field in models.APIConfiguration._meta.fields]
