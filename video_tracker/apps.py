from django.apps import AppConfig


class VideoTrackerConfig(AppConfig):
    name = 'Video Tracker'
