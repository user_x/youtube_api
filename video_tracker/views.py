import asyncio
import threading

from django.shortcuts import render
from django.db.models import Q
from django import forms

from rest_framework.renderers import JSONRenderer
from rest_framework import generics, status, filters
from rest_framework.views import APIView
from rest_framework.response import Response

from video_tracker import models
from video_tracker import serializers

from helpers import Pagination


class Video(generics.ListAPIView):
    """
    Returns videos with reverse chronological order of published date
    """
    model = models.Video
    serializer_class = serializers.VideoSerializer
    renderer_classes = [JSONRenderer]
    pagination_class = Pagination.DefaultPagination

    def get_queryset(self):
        """
        Filters videos if any search criteria found
        either over video title or/and description
        """
        api_keys = models.APIConfiguration.objects.filter(expired=False)
        queryset = self.model.objects.all()
        if self.request.query_params.get('search'):
            keywords = self.request.query_params.get('search').split()
            q_search = Q(
                title__icontains=keywords[0]) | Q(description__icontains=keywords[0]
            )
            for keyword in keywords:
                q_search.add(
                    (
                        Q(title__icontains=keyword) | Q(description__icontains=keyword)
                    ), 
                    q_search.connector
                )
            queryset = queryset.filter(q_search)
        return queryset.order_by('-published_at')


class APIConfiguration(APIView):

    model = models.APIConfiguration
    serializer_class = serializers.APIConfigurationSerializer

    def get_queryset(self):
        """
        Returns API configurations queryset objects
        which yet not have expired
        """
        return self.model.objects.filter(expired=False)

    def get(self, request):
        """
        Returns all API configuration queryset objects as
        JSON response, if none found active raises ValidationError
        """
        queryset = self.get_queryset()
        if not queryset:
            raise forms.ValidationError("API Key(s) expired, please add new one")
        return Response(queryset.values())

    def post(self, request):
        """
        Gets or Creates record from/in API configuration model,
        & returns all the active querysets as JSON response
        """
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            raise forms.ValidationError(serializer.errors)
        api_config, created = self.model.objects.get_or_create(
            key=serializer.validated_data.get('key'),
            expired=serializer.validated_data.get('expired') or False
        )
        return Response(self.get_queryset().values())