import asyncio
import datetime
import threading
import time
from apiclient.discovery import build
from django.db import connections
from django.core.cache import cache

from video_tracker import models
from video_tracker.constants import PRE_DEFINED_QUERY
from utils import DateTimeConverter as date_util



def fetch_youtube_videos_data(query):
    """
    Latest videos fetched from youtube data API
    Using google-api-client:
        reference: https://github.com/youtube/api-samples/blob/master/python/search.py

    Args:
        query (str): search query criteria

    Returns:
        results (list): list of dictionaries with videos data fetched from response
    """
    api_configurations = models.APIConfiguration.objects.filter(expired=False)

    if not len(api_configurations):
        return {}

    kwargs = {}
    published_after = cache.get("published_after")

    if not published_after:
        latest_video_by_published_at = models.Video.objects.order_by('-published_at').last()
        published_after = (latest_video_by_published_at 
            and date_util.to_str_with_tz(latest_video_by_published_at.published_at)
        )

    if published_after:
        kwargs = {
            'publishedAfter': published_after
        }

    DEVELOPER_KEY = api_configurations[0]
    YOUTUBE_API_SERVICE_NAME = 'youtube'
    YOUTUBE_API_VERSION = 'v3'
    
    try:
        youtube = build(
            YOUTUBE_API_SERVICE_NAME, 
            YOUTUBE_API_VERSION, 
            developerKey=DEVELOPER_KEY
        )
        """
        Returns max. 25 results by default
        """
        search_keyword = youtube.search().list(
            q=query, part="id, snippet",
            **kwargs
        ).execute()

        results = search_keyword.get("items", [])
    except:
        api_configurations[0].expired = True
        api_configurations[0].save()
        return {}

    return results


def save_video_and_thumbnails(result, latest_published_at):
    """
    Saving video and thumbnails

    Args:
        result (dict): list of video data dictionaries
        mapped as per model
        latest_published_at (str): date-time string for latest uploaded video
    """
    video_dict = {
        'title': result["snippet"]["title"],
        'description': result['snippet']['description'],
        'published_at': date_util.to_date_time(
            result['snippet']['publishedAt']),
    }
    video_obj = models.Video(**video_dict)
    video_obj.save()
    cache.set(key='published_after', value=latest_published_at)

    thumbnails = [{
        'url':  result['snippet']['thumbnails'][screen_size]['url'],
    } for screen_size in result['snippet']['thumbnails']]

    for thumbnail in thumbnails:
        thumbnail['video'] = video_obj
        thumbnail_obj = models.VideoThumbnail(**thumbnail)
        thumbnail_obj.save()

    for conn in connections.all():
        conn.close()


async def youtube_videos_service():
    """
    Async service for fetching video data from youtube data API
    function, storing results in db.
    """
    while True:
        response = fetch_youtube_videos_data(PRE_DEFINED_QUERY)

        if response == {}:
            return

        latest_published_at = ''

        for result in response:
            try:
                latest_published_at = max(
                    latest_published_at, result['snippet']['publishedAt']
                )
            except:
                latest_published_at = result['snippet']['publishedAt']
            save_video_and_thumbnails(result, latest_published_at)
        await asyncio.sleep(10)


def run_youtube_videos_service():
    """
    Start services for searching and adding youtube videos
    """
    while True:
        api_configurations = models.APIConfiguration.objects.filter(expired=False)
        if len(api_configurations):
            asyncio.run(youtube_videos_service())
        time.sleep(10)

THREAD = threading.Thread(target=run_youtube_videos_service)