from django.urls import path

from video_tracker import views

urlpatterns = [
	path('get-videos', views.Video.as_view(), name='get-videos'),
	path('api-config', views.APIConfiguration.as_view(), name='api-config'),
]

from video_tracker.services import THREAD
THREAD.start()