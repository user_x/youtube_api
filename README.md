# README #

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact# Youtube Data API Integration

Technologies -  
1. Python  
2. Django  
3. SQLite

## Postman collection
[postman-collection](https://www.getpostman.com/collections/105240d9ad42596e4fe2) 


Setup Guide  
1. Clone the Repository  
2. Create virtual environment and active the same  
3. Move the youtube_api django project that is cloned at step number 1  
4. Install dependencies required for the running the project, with the command below    
        ```
            $ pip install -r requirements.txt    
        ```    
5. db.sqlite3 file already contains the required data. The same data can be preferred to run the project with superuser - Email: test@test.com, Username: test, Password: Test@123 for admin dashboard access. Otherwise, to generate data and tables manually from level 0, delete db.sqlite3 file present in directory youtube_api (project) and run command below -    
        ```
            $ python3 manage.py migrate  
        ```    
6. Now create superuser for accessing the dashboard to view the stored videos    
        ```
            $ python3 manage.py createsuperuser  
        ```    
7. Now run the following command for starting the server
        ```
            $ python3 manage.py runserver    
        ```    
8. Open http://127.0.0.1:8000/admin for the admin dashboard, access it with the given/created superuser.  
9. Test APIs given in the postman collection linked above.